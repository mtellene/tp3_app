package com.example.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;



import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TeamActivity extends AppCompatActivity{

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    private boolean add;
    private boolean update;
    private long id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        lastEvent = new Match();

        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        add = (boolean) getIntent().getExtras().getBoolean("add");

        id = team.getId();

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new LongOperation().execute();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        intent.putExtra(Team.TAG, team);
        setResult(1, intent);
        finish();
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        new AsyncTaskImage().execute();
    }

    private class LongOperation extends AsyncTask<TextView, String, Team>{

        AlertDialog.Builder alertDialogBuilder;

        protected void onPreExecute(){
            super.onPreExecute();
            alertDialogBuilder = new AlertDialog.Builder(TeamActivity.this);
        }

        @Override
        protected Team doInBackground(TextView... textViews) {
            boolean run = true;
            while(run) {
                URL url = null;
                HttpURLConnection urlConnection = null;
                try {
                    url = WebServiceUrl.buildSearchTeam(team.getName());
                    urlConnection = (HttpURLConnection) url.openConnection();
                    JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(team);
                    try {
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        jsonTeam.readJsonStream(is);
                        url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                        urlConnection = (HttpURLConnection) url.openConnection();
                    } finally {
                        if (!jsonTeam.exist){
                            this.cancel(true);
                            if(isCancelled()){
                                break;
                            }
                        }
                        urlConnection.disconnect();
                    }
                    try {
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        JSONResponseHandlerLeague jsonLeague = new JSONResponseHandlerLeague(team);
                        jsonLeague.readJsonStream(is);
                        url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                        urlConnection = (HttpURLConnection) url.openConnection();
                    } finally {
                        urlConnection.disconnect();
                    }
                    try {
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        JSONResponseHandlerEvent jsonEvent = new JSONResponseHandlerEvent(lastEvent, team);
                        jsonEvent.readJsonStream(is);
                    } finally {
                        urlConnection.disconnect();
                        run = false;
                    }
                } catch (MalformedURLException e) { //for buildSearchTeam
                    e.printStackTrace();
                } catch (IOException e) {   //for openConnection
                    e.printStackTrace();
                }
            }
            return team;
        }

        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
            updateView();
        }

        protected void onCancelled(){
            alertDialogBuilder.setTitle("Equipe inexistante");
            alertDialogBuilder.setMessage("Veuillez supprimer cette équipe !");
            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }
    }

    private class AsyncTaskImage extends AsyncTask<String,Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try{
                URL url = new URL(team.getTeamBadge());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    bitmap = BitmapFactory.decodeStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bm){
            super.onPostExecute(bm);
            imageBadge.setImageBitmap(bm);
        }
    }
}
