package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import static java.sql.Types.NULL;

public class JSONResponseHandlerEvent {

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=idTeam
 * Responses must be provided in JSON.
 *
 */


    private static final String TAG = JSONResponseHandlerLeague.class.getSimpleName();

    private Match match;
    private Team team;


    public JSONResponseHandlerEvent(Match match, Team team) {
        this.match = match;
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();   //asserts it is the begin of the reader
        while (reader.hasNext()) {  //hasNext() returns true if the reader has another element
            String name = reader.nextName();    //nextName() returns the next token (JsonToken)
            if (name.equals("results")) { //if nextName() returns the String "table"
                readArrayTeams(reader); //call readArrayTeams on the reader
            } else {    //if nextName() does not return the String "teams"
                reader.skipValue(); //skip the value
            }
        }
        reader.endObject(); //asserts it is the end of the reader
    }
    //fonction maj : lastMatch
    //recupere : labelMatch, homeTeam, awayTeam, homeScore, awayScore
    //nb -> under team in : https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=idTeam
    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();    //asserts it is the begin of the array
        int nb = 0; // only consider the first element of the array,
        while (reader.hasNext() ) {
            reader.beginObject();
            while(reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idEvent")) { match.setId(reader.nextLong()); }
                    else if (name.equals("strEvent")) { match.setLabel(reader.nextString()); }
                    else if (name.equals("strHomeTeam")) { match.setHomeTeam(reader.nextString()); }
                    else if (name.equals("strAwayTeam")) { match.setAwayTeam(reader.nextString()); }
                    else if (name.equals("intHomeScore")) {
                        if (reader.peek() != JsonToken.NULL) {  //si la bdd est en maj et que le pointeur est null
                            match.setHomeScore(reader.nextInt());
                        } else{
                            match.setHomeScore(-1);
                            reader.skipValue();
                        }
                    }
                    else if (name.equals("intAwayScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                        } else{
                            match.setAwayScore(-1);
                            reader.skipValue();
                        }
                    }
                    else { reader.skipValue(); }
                }  else {   //if nb != 0
                    reader.skipValue(); //skip the value
                }
            }
            reader.endObject();
            nb++;
        }
        team.setLastEvent(match);
        reader.endArray();
    }

}
