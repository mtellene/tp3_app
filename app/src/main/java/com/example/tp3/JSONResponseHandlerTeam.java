package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;

    public boolean exist = false;


    public JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();   //asserts it is the begin of the reader
        while (reader.hasNext()) {  //hasNext() returns true if the reader has another element
            String name = reader.nextName();    //nextName() returns the next token (JsonToken)
            if (name.equals("teams")) { //if nextName() returns the String "teams"
                readArrayTeams(reader); //call readArrayTeams on the reader
            } else {    //if nextName() does not return the String "teams"
                reader.skipValue(); //skip the value
            }
        }
        reader.endObject(); //asserts it is the end of the reader
    }
    //fonction maj : teamName, leagueName, stadiumName, stadiumLocationName, teamLogo
    //recupere : idTeam, idLeague
    //nb -> under team in : https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=TEAM
    private void readArrayTeams(JsonReader reader) throws IOException {
        if (reader.peek() != JsonToken.NULL) {
            reader.beginArray();    //asserts it is the begin of the array
            int nb = 0; // only consider the first element of the array,
            while (reader.hasNext()) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (nb == 0) {
                        if (name.equals("idTeam")) {
                            team.setIdTeam(reader.nextLong());
                        } else if (name.equals("strTeam")) {
                            team.setName(reader.nextString());
                        } else if (name.equals("strLeague")) {
                            team.setLeague(reader.nextString());
                        } else if (name.equals("idLeague")) {
                            team.setIdLeague(reader.nextLong());
                        } else if (name.equals("strStadium")) {
                            team.setStadium(reader.nextString());
                        } else if (name.equals("strStadiumLocation")) {
                            team.setStadiumLocation(reader.nextString());
                        } else if (name.equals("strTeamBadge")) {
                            team.setTeamBadge(reader.nextString());
                        } else {
                            reader.skipValue();
                        }
                    } else {   //if nb != 0
                        reader.skipValue(); //skip the value
                    }
                }
                reader.endObject();
                nb++;   //pass to the next object of the url page
            }
            reader.endArray();
            exist = true;
        }
    }

}
