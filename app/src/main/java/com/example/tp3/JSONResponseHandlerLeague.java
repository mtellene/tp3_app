package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=idLeague&s=year
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerLeague {

    private static final String TAG = JSONResponseHandlerLeague.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerLeague(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();   //asserts it is the begin of the reader
        while (reader.hasNext()) {  //hasNext() returns true if the reader has another element
            String name = reader.nextName();    //nextName() returns the next token (JsonToken)
            if (name.equals("table")) { //if nextName() returns the String "table"
                readArrayTeams(reader); //call readArrayTeams on the reader
            } else {    //if nextName() does not return the String "teams"
                reader.skipValue(); //skip the value
            }
        }
        reader.endObject(); //asserts it is the end of the reader
    }
    //fonction maj : ranking, total
    //nb -> under team in : https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=idLeague&s=year
    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();    //asserts it is the begin of the array
        int nb = 0; // only consider the first element of the array,
        while (reader.hasNext() ) {
            reader.beginObject();
            while(reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("teamid")){
                    long teamid = reader.nextLong();
                    if(teamid == team.getIdTeam()){
                        team.setRanking(nb+1);
                        while(reader.hasNext()){
                            String s = reader.nextName();
                            if(s.equals("total")){ team.setTotalPoints(reader.nextInt()); }
                            else{ reader.skipValue(); }
                        }
                    }
                }else{ reader.skipValue(); }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
