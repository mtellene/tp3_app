package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    final String[] colFrom = new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME };

    public SportDbHelper dbHelper;
    public SimpleCursorAdapter adapter;
    public ListView listView;
    private SwipeRefreshLayout refresh;
    private  SwipeDetector swipeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new SportDbHelper(this);
        //dbHelper.dropTable(dbHelper.getReadableDatabase());
        //empeche le populate si il y a des trucs dans la db
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + SportDbHelper.TABLE_NAME, null);
        if(mCursor.getCount() == 0) {
            dbHelper.populate();
        }

       final Cursor cursor = dbHelper.fetchAllTeams();

       adapter = new SimpleCursorAdapter(
               this,
               android.R.layout.simple_list_item_2,
               cursor,
               colFrom,
               new int[]{ android.R.id.text1, android.R.id.text2},
               0);

       listView = (ListView)findViewById(R.id.listOfTeam);
       listView.setAdapter(adapter);

       swipeDetector = new SwipeDetector();
       listView.setOnTouchListener(swipeDetector);
       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Cursor cursor = (Cursor) parent.getItemAtPosition(position);
               Team teamWanted = dbHelper.cursorToTeam(cursor);
               if(swipeDetector.swipeDetected()){
                   if(swipeDetector.getAction()==Action.LR){
                       int idTeam = (int) teamWanted.getId();
                       dbHelper.deleteTeam(idTeam);
                       adapter.changeCursor(dbHelper.fetchAllTeams());
                       adapter.notifyDataSetChanged();
                       Toast.makeText(MainActivity.this, "Suppression de " + teamWanted.getName(), Toast.LENGTH_LONG).show();
                   } else if(swipeDetector.getAction()==Action.RL){
                       int idTeam = (int) teamWanted.getId();
                       dbHelper.deleteTeam(idTeam);
                       adapter.changeCursor(dbHelper.fetchAllTeams());
                       adapter.notifyDataSetChanged();
                       Toast.makeText(MainActivity.this, "Suppression de " + teamWanted.getName(), Toast.LENGTH_LONG).show();
                   }
               }else {
                   Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                   intent.putExtra(Team.TAG, (Parcelable) teamWanted);
                   startActivityForResult(intent, 1);
               }
           }
       });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team teamCreated = new Team("");
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                intent.putExtra(Team.TAG, (Parcelable) teamCreated);
                intent.putExtra("add",true);
                startActivityForResult(intent,1);
            }
        });

        refresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AsyncTaskRefresh().execute();
            }
        });
    }

    public enum Action {
        LR, // Left to Right
        RL, // Right to Left
        None // when no action was detected
    }

    public class SwipeDetector implements View.OnTouchListener {

        private static final String logTag = "SwipeDetector";
        private static final int MIN_DISTANCE = 100;
        private float downX, downY, upX, upY;
        private Action mSwipeDetected = Action.None;

        public boolean swipeDetected() {
            return mSwipeDetected != Action.None;
        }

        public Action getAction() {
            return mSwipeDetected;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    mSwipeDetected = Action.None;
                    return false; // allow other events like Click to be processed
                }
                case MotionEvent.ACTION_MOVE: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    // horizontal swipe detection
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            mSwipeDetected = Action.LR;
                            return true;
                        }
                        if (deltaX > 0) {
                            mSwipeDetected = Action.RL;
                            return true;
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataIntent){
        super.onActivityResult(requestCode,resultCode,dataIntent);
        if(dataIntent!= null) {
            Team team = dataIntent.getParcelableExtra(Team.TAG);
            boolean add = (dataIntent.getExtras()).getBoolean("add");
            assert team != null;
            if (add) {
                boolean insert = dbHelper.addTeam(team);
                if(insert) {
                    adapter.changeCursor(dbHelper.fetchAllTeams());
                    adapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Une équipe portant le même nom existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
            } else { dbHelper.updateTeam(team); }
        }
        adapter.changeCursor(dbHelper.fetchAllTeams());
        //refresh the listView
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AsyncTaskRefresh extends AsyncTask<TextView, String, List<Team>> {

        private List<Team> listTeam;

        @Override
        protected List<Team> doInBackground(TextView... textViews) {
            listTeam = dbHelper.getAllTeams();
            for(Team team : listTeam){
                boolean run = true;
                while(run) {
                    URL url = null;
                    HttpURLConnection urlConnection = null;
                    try {
                        url = WebServiceUrl.buildSearchTeam(team.getName());
                        urlConnection = (HttpURLConnection) url.openConnection();
                        JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(team);
                        try {
                            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                            jsonTeam.readJsonStream(is);
                            url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                            urlConnection = (HttpURLConnection) url.openConnection();
                        } finally {
                            if (!jsonTeam.exist){
                                break;
                            }
                            urlConnection.disconnect();
                        }
                        try {
                            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerLeague jsonLeague = new JSONResponseHandlerLeague(team);
                            jsonLeague.readJsonStream(is);
                            url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                            urlConnection = (HttpURLConnection) url.openConnection();
                        } finally {
                            urlConnection.disconnect();
                        }
                        try {
                            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                            Match lastEvent = new Match();
                            JSONResponseHandlerEvent jsonEvent = new JSONResponseHandlerEvent(lastEvent, team);
                            jsonEvent.readJsonStream(is);
                        } finally {
                            urlConnection.disconnect();
                            run = false;
                        }
                    } catch (MalformedURLException e) { //for buildSearchTeam
                        e.printStackTrace();
                    } catch (IOException e) {   //for openConnection
                        e.printStackTrace();
                    }
                }
            }
            return listTeam;
        }

        @Override
        protected void onPostExecute(List<Team> listTeam) {
            super.onPostExecute(listTeam);
            for(Team team : listTeam){
                dbHelper.updateTeam(team);
            }
            adapter.changeCursor(dbHelper.fetchAllTeams());
            adapter.notifyDataSetChanged();
            refresh.setRefreshing(false);
        }
    }
}
